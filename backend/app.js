const express = require('express'),
  app = express(),
  SiteRoutes = require('./config/routes'),
  SitePolicy = require('./config/policy');

module.exports.start = (callback) => {
  app.listen(3001, init);
}

function init() {
  SitePolicy.init();
  SiteRoutes.init(app);
}
