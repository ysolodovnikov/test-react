const acl = require('acl'),
  _ = require('lodash'),
  bluebird = require('bluebird');

let instance = null;
let allow,
  areAnyRolesAllowed,
  whatResources;

class SitePolicy {

  constructor() {
    if (!instance) {
      instance = this;
      instance.acl = new acl(new acl.memoryBackend());
      allow = bluebird.promisify(instance.acl.allow).bind(instance.acl);
      areAnyRolesAllowed = bluebird.promisify(instance.acl.areAnyRolesAllowed).bind(instance.acl);
      whatResources = bluebird.promisify(instance.acl.whatResources).bind(instance.acl);
    }
    return instance;
  }

  async init() {
    await allow([{
      roles: ['guest'],
      allows: [{
        resources: 'site',
        permissions: ['site:guest']
      }]
    },
    {
      roles: ['user'],
      allows: [{
        resources: 'site',
        permissions: ['site:use']
      }]
    },
    {
      roles: ['admin'],
      allows: [{
        resources: 'site',
        permissions: ['site:administer']
      }]
    }]);

    /** Site Hierarchy */
    instance.acl.addRoleParents('admin', 'user');
    instance.acl.addRoleParents('user', 'guest');
    /**End Of Site Hierarchy */
  }

  async getPermissions(userRoles, resource = 'site') {
    let allowedPermissions = await whatResources(userRoles);

    return allowedPermissions[resource]; // ignore list of other api permissions
  }

  async isAllowed(roles, options) {
    roles = [
      'guest',
      ...roles
    ];
    let allowed = await areAnyRolesAllowed(roles, options.resource, options.permissions);
    return allowed;
  }

  auth(options) {
    return async (req, res, next) => {
      let roles = [];
      if (req.user) {
        roles = _.concat(roles, req.user.roles);
      }

      const allowed = await instance.isAllowed(roles, options);

      if (allowed) {
        return next();
      }
      return res.status(403).json({
        message: 'Permission denied. You dont\'t have access for this operation.'
      });
    };
  }

}

module.exports = new SitePolicy();

