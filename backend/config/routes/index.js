const coreRoutes = require('./../../modules/core/routes'),
  SitePolicy = require('./../policy');

module.exports.init = (app) => {
  coreRoutes(app, SitePolicy);
};
