const core = require('./../controller/core.controller.server');

module.exports = function (app, SitePolicy) {
  app.route('/')
  .get(SitePolicy.auth({
    resource: 'site',
    permissions: ['site:guest']
  }), core.home)
};
