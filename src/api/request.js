import { camelizeKeys } from 'humps';

// Shorthand error response return construct.
const errorResponse = (error, headers) => ({ error, headers });

const request = ({
  url,
  body,
  headers,
  method = 'GET'
}) => {
  const args = { body, headers, method };
  return fetch(url, args)
    .then(response => response.json().then(json => ({ json, response })))
    .then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject({ json, headers: response.headers });
      }

      return {
        headers: response.headers,
        response: camelizeKeys(json)
      };
    })
    .catch(({ headers, message }) =>
      errorResponse(message, headers)
    );
};

const get = (...passThroughArgs) => request(...passThroughArgs);

const post = ({ data, ...passThroughArgs }) => {
  let args = { ...passThroughArgs, method: 'POST' };

  // Add body and headers for posts with data.
  if (data) {
    args = {
      ...args,
      body: JSON.stringify(data),
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      }
    };
  }

  return request(args);
};

export default { get, post };
