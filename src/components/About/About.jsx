import React, { Component } from 'react';
import './About.scss';
import { connect } from "react-redux";

class About extends Component {
  constructor(props){
    super(props);
    
    console.log('about - ', this)
  }

  // componentWillMount(){}
  // componentDidMount(){}
  // componentWillUnmount(){}

  // componentWillReceiveProps(){}
  // shouldComponentUpdate(){}
  // componentWillUpdate(){}
  // componentDidUpdate(){}

  render() {
    return (
      <div>
        About Page
      </div>
    );
  }
}

export default connect(
  state => ({
    appStore: state
  }),
  dispatch => ({})
)(About)