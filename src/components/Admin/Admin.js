import React, { Component } from 'react';
import { Admin, Resource, ListGuesser } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';

import PostIcon from '@material-ui/icons/Book';
import UserIcon from '@material-ui/icons/Group';
import { createMuiTheme } from '@material-ui/core/styles';
import './Admin.scss';

import { PostList, PostCreate, PostEdit } from './components/posts'
import dashboard from './components/dashboard'
import authProvider from './components/authProvider'

const theme = createMuiTheme({
  palette: {
    // type: 'dark', // Switching the dark mode on is a single property value change.
  },
});


class AdminPage extends Component {
  // constructor(props){
    // super(props);
    // this.state = {};
  // }

  // componentWillMount(){}
  // componentDidMount(){}
  // componentWillUnmount(){}

  // componentWillReceiveProps(){}
  // shouldComponentUpdate(){}
  // componentWillUpdate(){}
  // componentDidUpdate(){}

  render() {
    const dataProvider = jsonServerProvider('http://jsonplaceholder.typicode.com');
    return (
      <Admin theme={theme} dashboard={dashboard}  authProvider={authProvider} dataProvider={dataProvider}>
        <Resource name="users" list={ListGuesser} icon={UserIcon} />
        <Resource name="posts" icon={PostIcon} list={PostList} create={PostCreate} edit={PostEdit} />
      </Admin>
    );
  }
}

export default AdminPage;