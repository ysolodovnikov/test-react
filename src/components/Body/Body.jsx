import React, { Component } from 'react';
import './Body.scss';
import globalTranslations from '../../translations/global'
import { withLocalize } from "react-localize-redux";
import { Translate } from "react-localize-redux";
import { Carousel } from 'react-bootstrap';

import slider1 from './src/images/slider1.jpg'
import slider2 from './src/images/slider2.jpeg'
import slider3 from './src/images/slider3.png'

class Body extends Component {

  constructor(props) {
    super(props);

    this.props.addTranslation(globalTranslations);
  }

  // componentWillMount(){}
  // componentDidMount(){}
  // componentWillUnmount(){}

  // componentWillReceiveProps(){}
  // shouldComponentUpdate(){}
  // componentWillUpdate(){}
  // componentDidUpdate(){}

  render() {
    // temp fix
    let slider = [slider1, slider2, slider3];

    const handleSelect = (selectedIndex, e) => {
      console.log(selectedIndex);
      console.log(e.direction);
    };

    return (
      <Carousel className="homeSlider" onSelect={handleSelect}>
        {slider.map((slide, i) => {
          return (<Carousel.Item key={i}>
            <img
              className="d-block w-100"
              src={slide}
              alt={i + ' slide'}
            />
            <Carousel.Caption>
              <h3>First slide label</h3>
              <a href="javascript:void(0)">Nulla vitae elit libero, a pharetra augue mollis interdum.</a>
            </Carousel.Caption>
          </Carousel.Item>)
        })}
      </Carousel>
    );
  }
}

export default withLocalize(Body);