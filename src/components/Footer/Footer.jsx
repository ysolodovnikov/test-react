import React, { Component } from 'react';
import { Translate } from "react-localize-redux";
import './Footer.css';

class Footer extends Component {
  // constructor(props){
  // super(props);
  // this.state = {};
  // }

  // componentWillMount(){}
  // componentDidMount(){}
  // componentWillUnmount(){}

  // componentWillReceiveProps(){}
  // shouldComponentUpdate(){}
  // componentWillUpdate(){}
  // componentDidUpdate(){}

  render() {
    return (
      <div className="footer container">
        <div className="left-side footer-caption">
          <Translate id="config.title" /> © {(new Date().getFullYear())}.
        </div>
        <div className="right-side">
        </div>

      </div>
    );
  }
}

export default Footer;