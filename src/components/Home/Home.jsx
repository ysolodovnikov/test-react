import React, { Component } from 'react';
import './Home.scss';
import Body from '../Body'

import { withLocalize } from "react-localize-redux";

class Home extends Component {

  // componentWillMount(){}
  // componentDidMount(){}
  // componentWillUnmount(){}

  // componentWillReceiveProps(){}
  // shouldComponentUpdate(){}
  // componentWillUpdate(){}
  // componentDidUpdate(){}

  render() {
    return (
      <div>
        <Body />
      </div>
    );
  }
}

export default withLocalize(Home);
