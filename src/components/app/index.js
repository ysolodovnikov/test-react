import React from 'react';
import { withLocalize } from "react-localize-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { connect } from "react-redux";

import './App.scss';
import Home from '../Home'
import SiteHeader from '../header/header'
import Footer from '../Footer'
import About from '../About'
import NotFound from '../NotFound'
// import request from '../../api/request'
import Admin from '../Admin'
import globalTranslations from '../../translations/global'
import { renderToStaticMarkup } from "react-dom/server";




class App extends React.Component {

  constructor(props) {
    super(props);

    const defaultLanguage =
      localStorage.getItem('languageCode') || props.languages[0];

    this.props.initialize({
      languages: [
        { name: 'English', code: 'en' },
        { name: 'Українська', code: 'ua' },
        { name: 'Русский', code: 'ru' }
      ],
      translation: globalTranslations,
      options: {
        renderToStaticMarkup,
        defaultLanguage
      }
    });
  }

  async componentDidMount() {
    // const response = await request.get({
    //   url: 'https://comdev.broadcast.promethean.tv/api/platforms'
    // });
    // console.log('response - ', response);
    // this.setState({ data: response });
  }

  // request.get('https://comdev.broadcast.promethean.tv/api/platforms')

  render() {
    // store.dispatch({type: 'ADD_SLIDER_IMAGE', image: '2.jpg'})
    console.log(this);
    return (
      
        <Router>
          <div className="app">
            <SiteHeader />
            <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/about' component={About} />
              <Route path='/admin' component={Admin} />
              <Route component={NotFound} />
            </Switch>
            <Footer />
          </div>
        </Router>
      
    );
  }
}

export default connect(
  state => ({
    appStore: state
  }),
  dispatch => ({})
)(withLocalize(App))