import React from 'react';
import { withLocalize, Translate } from "react-localize-redux";
import { Navbar, Nav, NavDropdown, FormControl, Form, Button } from 'react-bootstrap';
import { connect } from "react-redux";
import './header.scss'

class SiteHeader extends React.Component {

  componentDidUpdate(prevProps) {
    const prevLangCode = prevProps.activeLanguage && prevProps.activeLanguage.code;
    const curLangCode = this.props.activeLanguage && this.props.activeLanguage.code;
    const hasLanguageChanged = prevLangCode !== curLangCode;

    if (hasLanguageChanged) {
      localStorage.setItem('languageCode', curLangCode);
    }
  }

  setDefaultLanguage(lang, e) {
    this.props.setActiveLanguage(lang)
  }

  render() {
    console.log('header - ', this.props.appStore)
    return (
      <Navbar bg="light" expand="lg" className="justify-content-start header">
        <Navbar.Brand href="/" className="col-lg-8 col-md-10 margin-0">
          <h1 className="text-left" >
            <Translate id="config.title" />
          </h1>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" className="new-navbar-toogle justify-content-end col-md-2 text-right" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end new-navbar-collapse col-lg-4">
          <Nav>
            <Nav.Link href="/">
              <Translate id="header.home" />
            </Nav.Link>
            <Nav.Link href="/about">
              <Translate id="header.about" />
            </Nav.Link>
            <NavDropdown id="basic-nav-dropdown" title={this.props.translate('config.language')}>
              {this.props.languages.map((lang) =>
                <NavDropdown.Item key={lang.code} onClick={(e) => this.setDefaultLanguage(lang.code, e)}>{lang.name}</NavDropdown.Item>
              )}
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default connect(
  appState => ({
    appStore: appState,
  }),
  dispatch => ({})
)(withLocalize(SiteHeader))