import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './components/app'
import * as serviceWorker from './serviceWorker';
// import { composeWithDevTools } from "redux-devtools-extension";

import rootReducer from './reducers'
import { createStore } from "redux";
import { Provider } from "react-redux";
import { LocalizeProvider } from "react-localize-redux";

let initialState = {

}

const store = createStore(
  rootReducer, // create redux store using root reducer
  initialState = {}, // initial state of the application
  // composeWithDevTools(
    // applyMiddleware(
      // router and other middlewares
    // )
  // )
);

ReactDOM.render(
  <Provider store={store}>
    <LocalizeProvider store={store}>
      <App />
    </LocalizeProvider>
  </Provider>,
  document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
