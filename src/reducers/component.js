const initState = ['Header', 'Footer']

const sliderList = (state = initState, action) => {
  switch (action.type) {
    case 'HIDE_HEADER':
      return [
        ...state
      ]
    default:
      return state
  }
}

export default sliderList