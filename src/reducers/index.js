import { combineReducers } from 'redux'
import sliderList from './sliderList'
import { localizeReducer } from "react-localize-redux";

export default combineReducers({
  localize: localizeReducer,
  sliderList
})