
const sliderList = (state = ['1.png'], action) => {
  switch (action.type) {
    case 'ADD_SLIDER_IMAGE':
      console.log(action);
      return [
        ...state,
        action.image
      ]
    default:
      return state
  }
}

export default sliderList